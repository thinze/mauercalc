<?php

?>

<fieldset id="wandgroesse" data-role="controlgroup">
    <legend>Wandmaße</legend>
    <h3 class="heading">Breite</h3>
    <div class="group-2col ui-field-contain">
        <select name="m-breite" id="m-breite">
            <?php
            foreach ( range(0, 40) as $x) {
                echo '<option value="' . $x . '">' . $x .'</option>';
            }
            ?>
        </select>
        <select name="cm-breite" id="cm-breite">
            <?php
            foreach ( range(0, 99) as $x) {
                echo '<option value="' . $x . '">' . $x .'</option>';
            }
            ?>
        </select>
    </div>
    <h3 class="heading">Höhe</h3>
    <div class="group-2col ui-field-contain">
        <select name="m-hoehe" id="m-hoehe">
            <?php
            foreach ( range(0, 10) as $x) {
                echo '<option value="' . $x . '">' . $x .'</option>';
            }
            ?>
        </select>
        <select name="cm-hoehe" id="cm-hoehe">
            <?php
            foreach ( range(0, 99) as $x) {
                echo '<option value="' . $x . '">' . $x .'</option>';
            }
            ?>
        </select>
    </div>
</fieldset>

<fieldset id="fuge" data-role="controlgroup">
    <legend>Fugendicke (mm)</legend>
    <div>
        <select name="fugendicke" id="fugendicke">
            <?php
            foreach ( range(1, 50) as $x) {
                echo '<option value="' . $x . '">' . $x .'</option>';
            }
            ?>
        </select>
    </div>
</fieldset>

<fieldset id="steine" data-role="controlgroup">
    <legend>Steinart</legend>
    <select name="steintyp" id="steintyp">
        <option selected="selected" value="0">bitte wählen</option><?php
        include('steine.inc.php');
        foreach($steintypen as $steinart) {
          echo '<optgroup label="' . $steinart['name'] .'">';
          foreach ($steinart['types'] as $stein) {
              echo '<option value="' . $stein['type'] . '">' . $stein['name'] . '</option>';
          }
            echo '</optgroup>';
        }
        ?>
    </select>
    <script><?php echo 'var steine = ' . json_encode($steintypen) . ';' ?></script>
</fieldset>

