<!DOCTYPE html>
<html lang="de" class="ui-mobile">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Wand- & Mauer-Calculator</title>
  <meta name="description" content="...">
  <meta name="author" content="HiDeVis">

  <link rel="stylesheet" href="/libs/jquery.mobile/themes/jquery.mobile-1.4.5.min.css">
  <link rel="stylesheet" href="/libs/jquery.mobile/themes/jquery.mobile.icons.min.css">
  <link rel="stylesheet" href="/css/styles.css?v=<?= rand(1, 9999) ?>">

</head>

<body data-role="page">

  <div data-role="header">
    <h1>Mauer-Rechner</h1>
  </div>

  <div role="main" class="ui-content">

    <?php include('form.inc.php'); ?>

    <div class="ui-field-contain">
      <button class="ui-btn ui-btn-active" id="exec-calc">berechnen</button>
      <section id="calc-result">
        <label for="result">Steinzahl</label>
        <input id="result" name="result" value="0">
      </section>
      <section id="result-details">
        <table>
          <tbody>
          <tr>
            <th>&nbsp;</th>
            <th class="spacer"><!-- --></th>
            <th>&nbsp;</th>
          </tr>
          <tr id="res-horiz">
            <td>horizontal</td>
            <td></td>
            <td></td>
          </tr>
          <tr id="res-vert">
            <td>vertikal</td>
            <td></td>
            <td></td>
          </tr>
          <tr id="res-price">
            <td>Kosten</td>
            <td></td>
            <td><span></span> €</td>
          </tr>
          </tbody>
        </table>
      </section>
    </div>

  </div>

  <div id="footer" data-role="footer">
    <div>
      Haben Sie Anmerkungen?
      <div class="ui-input-btn ui-btn ui-corner-all ui-shadow" id="mailto-btn">
        <a href="mailto:hidevis@gmx.de">Feedback senden</a>
      </div>
    </div>
  </div>

<script src="/libs/jquery/jquery-1.11.3.min.js"></script>
<script src="/libs/jquery.mobile/jquery.mobile-1.4.5.min.js"></script>
<script src="/js/mauercalc.js?v=<?= rand(1, 9999) ?>"></script>
</body>
</html>