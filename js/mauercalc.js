(function($) {

    function initPage() {
        var calc = $('#exec-calc').click(updateResult);
    }

    function updateResult() {
        var m_b         = parseInt($('#m-breite').val()) * 100 + parseInt($('#cm-breite').val());
        var m_h         = parseInt($('#m-hoehe').val()) * 100 + parseInt($('#cm-hoehe').val());
        var fuge        = parseInt($('#fugendicke').val());
        var steintyp    = $('#steintyp').val();
        var result      = calcStones(m_b, m_h, fuge, steintyp);
        $('#result').val(result);
    }

    function calcStones(m_b, m_h, fuge, steintyp) {
        var result = null;
        var stein  = getSteinInfo(steintyp);
        if (stein) {
            // all values as millimeter
            var s_horiz = Math.ceil(m_b * 10 / (stein.width + fuge));
            var s_vert = Math.ceil(m_h * 10 / (stein.height + fuge));
            result = s_horiz * s_vert;
            updateResultDetails(s_horiz, s_vert, result);
        } else {
            result = 'Fehler';
        }
        return result;
    }

    function getSteinInfo(steintyp) {
        var stein = null;
        if (steintyp.indexOf('df') !== -1) {
            stein = steine.df.types[steintyp];
        } else if (steintyp.indexOf('nf') !== -1) {
            stein = steine.nf.types[steintyp];
        }
        return stein;
    }

    function updateResultDetails(h, v, cnt) {
        $('#result-details').addClass('show');
        var dummy_price = 2.95 * cnt;
        $('#res-horiz td:nth-child(3)').text(h);
        $('#res-vert td:nth-child(3)').text(v);
        $('#res-price td:nth-child(3) > span').text(dummy_price.toFixed(2));
    }

    // DOM ready
    $(function() {
        initPage();
    });

})(jQuery);