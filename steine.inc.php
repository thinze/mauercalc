<?php

$steintypen = [

    'nf' => [
        'name'      => 'NF-Typen',
        'types'     => [

            '1nf' => [
                'type'      => '1nf',
                'name'      => '1NF',
                'width'     => 240,
                'height'    => 71,
                'depth'     => 115,
            ],
        ],
    ],

    'df' => [
        'name'      => 'DF-Typen',
        'types'     => [

            '1df' => [
                'type'      => '1df',
                'name'      => '1DF',
                'width'     => 240,
                'height'    => 52,
                'depth'     => 115,
            ],

            '2df' => [
                'type'      => '2df',
                'name'      => '2DF',
                'width'     => 240,
                'height'    => 113,
                'depth'     => 115,
            ],

            '3df' => [
                'type'      => '3df',
                'name'      => '3DF',
                'width'     => 240,
                'height'    => 113,
                'depth'     => 175,
            ],

            '4df' => [
                'type'      => '4df',
                'name'      => '4DF',
                'width'     => 240,
                'height'    => 115,
                'depth'     => 240,
            ],

            '5df' => [
                'type'      => '5df',
                'name'      => '5DF',
                'width'     => 300,
                'height'    => 115,
                'depth'     => 240,
            ],
    ],
    ],

];

?>